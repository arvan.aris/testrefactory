//Palindrome
function palindrome(word) {
    let lWord = word.toLowerCase()
    let temp = "";
    for (let i = word.length-1; i >= 0; i--) {
            temp += lWord[i]
    }
    
    if (temp == lWord) {
        console.log('Palindrome')
    }else{
        console.log('Not Palindrome')
    }
}

var sample = ["Radar", "Malam", "Kasur ini rusak", "Ibu Ratna antar ubi", 
            "Malas", "Makan nasi goreng", "Balonku ada lima"]
for (let i = 0; i < sample.length; i++) {
    palindrome(sample[i])
}

//Leap Year
function leapYear(tahunAwal, tahunAkhir) {
    let temp = []
    for (let i = tahunAwal+1; i <= tahunAkhir; i++) {
        if (i%4 == 0) {
            temp.push([i])
        }
    }
    console.log(temp.join(', '))
}
leapYear(1900, 2020)

//Reverse Word
function reverseWords(word) {
    const hasilSplit = word.split(" ")
    let temp = []
    for (let index = 0; index < hasilSplit.length; index++) {
        temp.push(reverse(hasilSplit[index]))
    }
    console.log(temp.join(" "))
}

function reverse(word) {
    let lowCase = word.toLowerCase()
    let temp = ""
    for (let i = word.length-1; i >= 0; i--) {
        temp += lowCase[i]
    }

    let hurufKapital = ""
    for (let i = 0; i < word.length; i++) {
        if (word[i] === word[i].toUpperCase()) {
            hurufKapital += temp.charAt(i).toUpperCase()
        }else{
            hurufKapital += temp.charAt(i)
        }
    }
    return hurufKapital
}
var kalimat = "I am A Great human"
console.log(kalimat)
console.log("into")
reverseWords(kalimat)

//Nearest Fibonacci
function nearestFibonacci(bil){
    var bilFibonacci = [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 69,
                        144, 233, 377, 610, 987]

    let jumArr = 0
    let nearFib = []
    for (let i = 0; i < bil.length; i++) {
        jumArr += bil[i];
    }
    
    for (let i = 0; i < bilFibonacci.length; i++) {
        nearFib.push(Math.abs(jumArr-bilFibonacci[i]))
    }
    console.log(Math.min(...nearFib))
}
let arr = [15, 1, 3]
nearestFibonacci(arr)

//FizzBuzz
function fizzBuzz(nilai) {
    let temp = []
    for (let i = 1; i <= nilai; i++) {
        if (i%3 === 0 && i%5 === 0) {
            temp.push('FizzBuzz')
        } else if (i%5 === 0) {
            temp.push('Buzz')
        } else if (i%3 === 0) {
            temp.push('Fizz')
        } else {
            temp.push([i])
        }
    }
    console.log(temp.join(", "))
}
const n = 15
fizzBuzz(n)