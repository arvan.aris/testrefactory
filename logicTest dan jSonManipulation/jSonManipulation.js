let data = [
    {
        "inventory_id": 9382,
        "name": "Brown Chair",
        "type": "furniture",
        "tags": [
            "chair",
            "furniture",
            "brown"
        ],
        "purchased_at": 1579190471,
        "placement": {
            "room_id": 3,
            "name": "Meeting Room"
        }
    },
    {
        "inventory_id": 9380,
        "name": "Big Desk",
        "type": "furniture",
        "tags": [
            "desk",
            "furniture",
            "brown"
        ],
        "purchased_at": 1579190642,
        "placement": {
            "room_id": 3,
            "name": "Meeting Room"
        }
    },
    {
        "inventory_id": 2932,
        "name": "LG Monitor 50 inch",
        "type": "electronic",
        "tags": [
            "monitor"
        ],
        "purchased_at": 1579017842,
        "placement": {
            "room_id": 3,
            "name": "Lavender"
        }
    },
    {
        "inventory_id": 232,
        "name": "Sharp Pendingin Ruangan 2PK",
        "type": "electronic",
        "tags": [
            "ac"
        ],
        "purchased_at": 1578931442,
        "placement": {
            "room_id": 5,
            "name": "Dhanapala"
        }
    },
    {
        "inventory_id": 9382,
        "name": "Alat Makan",
        "type": "tableware",
        "tags": [
            "spoon",
            "fork",
            "tableware"
        ],
        "purchased_at": 1578672242,
        "placement": {
            "room_id": 10,
            "name": "Rajawali"
        }
    }
]

let taskOne = data.filter(item => item.placement.name == "Meeting Room")
console.log('Find items in the Meeting Room.')
console.log(taskOne)

let taskTwo = data.filter(item => item.type == "electronic")
console.log(taskTwo)

let taskThree = data.filter(item => item.type == "furniture")
console.log(taskThree)

let TaskFour = data.filter(item => {
    let miliSecond = new Date(item.purchased_at*1000)
    let tahun = miliSecond.getFullYear()
    let namaBulan = ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agt", "Sep", "Okt", "Nov", "Des"]
    let bulan = namaBulan[miliSecond.getMonth()]
    let tanggal = miliSecond.getDate()
    if (tahun == 2020 && bulan == "Jan" && tanggal == 16) {
        return item
    }
})
console.log(TaskFour)

let taskFive = data.filter(item => item.tags.includes("brown"))
console.log(taskFive)
